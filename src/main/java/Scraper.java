import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.omg.PortableServer.THREAD_POLICY_ID;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class Scraper {
    public static void main(String[] args) throws IOException, InterruptedException {

        //SETUP WEBDRIVER - use your own path to webdriver
        System.setProperty("webdriver.chrome.driver", "E:/chromedriver.exe");
        WebDriver driver = new ChromeDriver();


        //LOGIN - use desired credentials
        driver.get("https://m.facebook.com/login.php");

        driver.findElement(By.name("email")).sendKeys("XXX@gmail.com");
        driver.findElement(By.name("pass")).sendKeys("XXX");

        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElement(By.name("login")).click();
        System.out.println("Login Successfull");
        Thread.sleep(3000);


/*  Use Cookies

        Map<String,String> cmap = new HashMap<String,String>();
        cmap.put("m_pixel_ratio", "1");
        for (Map.Entry<String, String> entry : cmap.entrySet()) {
            System.out.println(entry.getKey()+"  "+entry.getValue());
            driver.manage().addCookie(new Cookie(entry.getKey(), entry.getValue()));
        }
        driver.get("http://facebook.com/login/");
*/


        //GO TO MESSENGER & GET LINK OF LAST CONVERSATION
        driver.get("https://m.facebook.com/messages/");
        //System.out.println(driver.manage().getCookies());
        Thread.sleep(3000);

        String html = driver.getPageSource();
        Document doc = Jsoup.parse(html);

        Element threadList = doc.select("div#threadlist_rows").get(0);
        String link = threadList.select("a").attr("href");

        driver.get("https://m.facebook.com"+link);
        Thread.sleep(2000);



        //CLICK SEE OLDER UNTIL WHOLE CONVERSATION IS VISIBLE, max 10 times
        int counter = 10;
        while (counter>0) {
            try {
                WebElement we = driver.findElement(By.xpath("//div[@id='see_older']/a"));
                we.click();
                counter--;
                Thread.sleep(2000);
            }catch (Exception e){
                System.out.println("Break loop, whole conversation is visible");
                break;
            }
        }
        Thread.sleep(1000);


        //GET PAGE SOURCE AND CLOSE WEBDRIVER
        String convHtml = driver.getPageSource();
        driver.close();

        //Save page source to file
        /*
        PrintWriter writer = new PrintWriter("E:/the-file.txt", "UTF-8");
        writer.println(convHtml);
        writer.close();
        */


        //DISPLAY ALL MESSAGES
        Document convDoc = Jsoup.parse(convHtml);

        Elements msgs = convDoc.getElementsByAttributeValue("class", "_34ej");
        for (Element m : msgs) {
            //System.out.println(m.parent().select("span").text());
            if(m.parent().parent().hasClass("_1c_3") && m.parent().parent().hasClass("_34em")){
                System.out.print("Me: \t");
            }else{
                System.out.print("You: \t");
            }
            System.out.println(m.text());
        }

        System.out.println("<<< THE END >>>");
    }
}
